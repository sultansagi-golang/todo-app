package service

import (
	"github.com/sultansagi/todo-app"
	"github.com/sultansagi/todo-app/pkg/repository"
)

type Authorization interface {
	CreateUser(user todo.User) (int, error)
	GenerateToken(username, password string) (string, error)
	ParseToken(token string) (int, error)
}

type TodoList interface {
	Create(userId int, todoList todo.TodoList) (int, error)
	GetAll(userId int) ([]todo.TodoList,error)
	GetListById(userId, listId int) (todo.TodoList, error)
	DeleteById(userId, listId int) error
	Update(userId, listId int, input todo.UpdateListInput) error
}

type TodoItem interface {
	Create(userId, listId int, todoItem todo.TodoItem) (int, error)
	GetAll(userId, listId int) ([]todo.TodoItem, error)
}

type Service struct {
	Authorization
	TodoList
	TodoItem
}

func NewService(r *repository.Repository) *Service {
	return &Service{
		Authorization: NewAuthService(r.Authorization),
		TodoList: NewTodoListService(r.TodoList),
		TodoItem: NewTodoItemService(r.TodoItem, r.TodoList),
	}
}