package service

import (
	"github.com/sultansagi/todo-app"
	"github.com/sultansagi/todo-app/pkg/repository"
)

type TodoItemService struct {
	repo repository.TodoItem
	listRepo repository.TodoList
}

func NewTodoItemService(repo repository.TodoItem, listRepo repository.TodoList) *TodoItemService {
	return &TodoItemService{repo, listRepo}
}

func (s *TodoItemService) Create(userId, listId int, todoItem todo.TodoItem) (int, error) {
	if _, err := s.listRepo.GetListById(userId, listId); err != nil {
		return 0, err
	}
	return s.repo.Create(listId, todoItem)
}

func (s *TodoItemService) GetAll(userId, listId int) ([]todo.TodoItem, error) {
	return s.repo.GetAll(userId, listId)
}