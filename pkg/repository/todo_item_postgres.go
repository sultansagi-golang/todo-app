package repository

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/sultansagi/todo-app"
)

type TodoItemPostgres struct {
	db *sqlx.DB
}

func NewTodoItemPostgres(db *sqlx.DB) *TodoItemPostgres {
	return &TodoItemPostgres{db}
}

func (r *TodoItemPostgres) Create(listId int, todoItem todo.TodoItem) (int, error) {
	tx, err := r.db.Begin()
	if err != nil {
		return 0, err
	}

	var id int
	createItemQuery := fmt.Sprintf("INSERT INTO %s (title, description) VALUES ($1, $2) RETURNING id", todoItemsTable)
	row := r.db.QueryRow(createItemQuery, todoItem.Title, todoItem.Description)
	if err := row.Scan(&id); err != nil {
		tx.Rollback()
		return 0, err
	}
	createListItemsQuery := fmt.Sprintf("INSERT INTO %s (list_id, item_id) VALUES ($1, $2)", listItemsTable)
	_, err = tx.Exec(createListItemsQuery, listId, id)
	if err != nil {
		tx.Rollback()
		return 0, err
	}
	return id, tx.Commit()
}

func (r *TodoItemPostgres) GetAll(userId, listId int) ([]todo.TodoItem, error) {
	var items []todo.TodoItem
	query := fmt.Sprintf(`SELECT ti.* FROM %s ti INNER JOIN %s li ON ti.id = li.item_id 
		INNER JOIN %s ul ON li.list_id = ul.list_id WHERE ul.user_id=$1 AND li.list_id=$2`,
		todoItemsTable, listItemsTable, userListsTable)
	if err := r.db.Select(&items, query, userId, listId); err != nil {
		return nil, err
	}
	fmt.Println(items)
	return items, nil
}